package com.javagda23.files.read.przyklad;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class MainOdczytScanner {
    public static void main(String[] args) {
        // odczyt 1:
        // BufferedReader -- linia po linii
        // Scanner -- nextInt() nextDouble() hasNextInt()

        try (Scanner scanner = new Scanner(new FileReader("plik2.txt"))) {

            // dopóki jest jakaś linia w pliku
            while (scanner.hasNextLine()) {

                // wypisz ją
                System.out.println(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}