package com.javagda23.files.read.przyklad;

import java.util.Scanner;

public class MainScannerBlad {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj linie:");
        System.out.println(scanner.nextLine());

        System.out.println("Podaj slowo:");
        System.out.println(scanner.next());

        System.out.println("Podaj linie:");
        System.out.println(scanner.nextLine()); // pusta linia

    }
}
