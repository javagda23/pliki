package com.javagda23.files.write.przyklad;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;

public class MainZapis {
    public static void main(String[] args) {
        // File nie służy do zapisu/odczytu z pliku
        File plik = new File("/tmp/plik1.txt"); // File reprezentuje deskryptor pliku (zbiór informacji o pliku)

        try {
            if (!plik.exists()) {
                plik.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // zapis
        try (PrintWriter printWriter = new PrintWriter(new FileWriter("plik2.txt", true))){
            printWriter.println("def");

            ByteBuffer byteBuffer = ByteBuffer.allocate(8);
            byteBuffer.putLong(123L);

            for (byte b : byteBuffer.array()) {
                printWriter.write(b);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
